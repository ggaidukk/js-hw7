function filterBy(Array, argument) {
  type = typeof argument;
  let result = Array.filter((item) => typeof item != type);
  return result;
}
let Array = ["hello", "world", 23, "23", null, true, {}];
let argument = "string";

console.log(Array);
console.log(typeof argument);
console.log(filterBy(Array, argument));
